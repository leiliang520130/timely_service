<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

Route::get('think', function () {
    return 'hello,ThinkPHP5!';
});

Route::get('hello/:name', 'index/hello');
Route::rule('home', 'admin/Index/home');
Route::rule('user', 'admin/User/index');
Route::rule('kefuList', 'admin/Kefu/index');
Route::rule('addKefu', 'admin/Kefu/addKefu');
Route::rule('delKefu', 'admin/Kefu/delKefu');
Route::rule('updateKefu', 'admin/Kefu/updateKefu');
Route::rule('resetPassword', 'admin/Kefu/resetPassword');

Route::rule('visitorList', 'admin/Visitor/visitorList');
Route::rule('serviceList', 'admin/Visitor/serviceList');
Route::rule('chatLogList', 'admin/Visitor/chatLogList');
return [

];
